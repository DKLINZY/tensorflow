import tensorflow.compat.v1 as tf
import numpy as np


x_data = np.random.rand(100).astype(np.float32)
# weight 0.1; biases 0.3
y_data = x_data*0.1 + 0.3


Weights = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
biases = tf.Variable(tf.zeros([1]))

y = Weights*x_data + biases

loss = tf.reduce_mean(tf.square(y-y_data))

# learning <1
optimizer = tf.train.GradientDescentOptimizer(0.5)
train = optimizer.minimize(loss)

init = tf.global_variables_initializer()


sess = tf.Session()
sess.run(init)


for step in range(201):
    sess.run(train)
    if step % 20 == 0:
        print(step, sess.run(Weights), sess.run(biases))